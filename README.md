# COMP 490: Individual Primer Project
Mark Kaplan
September 7 2015
Version 1.0

Allows a user to search Wikipedia for <query> and display the results.

## Project Status
Fully functional.

## Install
pip install -r requirements.txt

## Log
Sep 3 - Initial simple CGI test.
Sep 5 - Add Jinja2 HTML/CSS template based on Bootstrap.
      - Add simple implementation of Wikipedia API.
Sep 7 - Switch to Wikipedia search API and display results.